﻿#000#Pędzel#Brush
#001#OGÓLNE#GENERAL
#002#LOSOWOŚĆ ODSTĘPÓW#SPACING JITTER
#003#KĄT#ANGLE
#004#KSZTAŁT PĘDZLA#BRUSH FORM
#005#KOLOR#COLOR
#006#TEKSTURA#TEXTURE
#007#Rozmiar#Size
#008#Kontrast tekstury#Tex. contrast
#009#Krycie#Opacity  (brush)
#010#Ilość koloru#Color amount
#011#Rozciągnięcie#Extension
#012#Stabilizacja#Stabilizer
#013#Przeźroczystość#Transparency
#014#Odstęp#Spacing
#015#Rozproszenie#Scatter
#016#Gęst. rozproszenia#Scat. density
#017#Kąt 1#Angle 1
#018#Kąt 2#Angle 2
#019#Zwęź#SqueezeX
#020#Spłaszcz#SqueezeY
#021#Poł. rozmiar#Connect sizes
#022#Obie osie#Both axis
#023#Stały#Fixation
#024#Użyj maski pędzla#Use mask brush
#025#Odwróć#Invert
#026#Próbkuj ze spodu#Pick under
#027#Mieszaj przeź.#Blend transp.
#028#Tryb brudzenia#Dirty mode
#029#Poł. rozmiarów#Size connection
#030#Chroń główny kolor#Preserve main color
#031#Los. skala#Rand scale
#032#Los. odstęp#Rand offset
#033#Odwróć#Invert
#034#Koło#Circle
#035#Tekstura#Custom form
#036#Gumka#Eraser
#037#Zwykły#Normal (blend mode)
#038#Pomnóż#Multiply
#039#Gęstość pędzla#Feature (Villuses count)
#040#Zestaw form#Set of forms
#041#Gradient#Gradient
#042#Użyj gradientu na linii#Use stroke gradient
#043#Radialny#Use radial
#044#Liniowy#Use linear
#045#Zróżnicowanie włosów#Villus diff.(relief villi)
#046#Skala#Scale
#047#Siła brudzenia#Dirty strength
#048#Użyj tekstury#Use texture
#049#Siła tekstury#Tex. strength
#050#Siła maski#Mask strength
#051# Zwykły# Normal
#052# Rozpuszczenie# Dissolve
#053# Ciemniej# Darken
#054# Mnożenie# Multiply
#055# Ściemnianie# Color Burn
#056# Ściemnianie lin.# Linear Burn
#057# Ciemniejszy# Darker Color
#058# Jaśniej# Lighten
#059# Ekran (Screen)# Screen
#060# Rozjaśnianie# Color Dodge
#061# Suma #Add
#062# Tylko jaśniejsze# Lighter Color
#063# Nakładka (Overlay)# Overlay
#064# Światło miękkie# Soft Light
#065# Światło twarde# Hard Light
#066# Światło jaskrawe# Vivid Light
#067# Światło liniowe# Linear Light
#068# Światło punktowe# Pin Light
#069# Mieszanie twarde# Hard Mix
#070# Różnica# Difference
#071# Wyłączenie# Exclusion
#072# Różnica# Substract
#073# Dzielenie# Divide
#074# Barwa# Hue
#075# Nasycenie# Saturation
#076# Kolor# Color
#077# Jasność# Luminosity
#078#Pędzle#Brushes
#079#Referencja#Reference
#080#Szerokość:#Width:
#081#Wysokość:#Height:
#082#Przerwij#Cancel
#083#Kolor#Color
#084#Cyjan-Czerwień#Cyan-red
#085#Fiolet-zieleń#Purple-green
#086#Żółty-niebieski#Yellow-blue
#087#Jasność#Brightness
#088#Kontrast#Contrast
#089#Cienie#Shadows
#090#Półcienie#Midtones
#091#Światła#Highlights
#092#Odcień#Hue
#093#Nasycenie#Saturation
#094#Jasność#Brihgtness
#095#Saturacja zgaszonego#Saturation of unsuturated
#096#Saturacja średnionasyconego#Saturation of medium suturated
#097#Saturacja nasyconego#Saturation of full suturated
#098#Własna paleta 1#Custom 1
#099#Własna paleta 2#Custom 2
#100#Własna paleta 3#Custom 3
#101#Narzędzia#Tools
#102#Pędzel#Brush
#103#Obraz#Canvas
#104#Warstwy i selekcje#Layers and selection
#105#Wygląd i skróty#Workspace
#106#Inne#Other
#107# - Puste -# -None-(Empty)
#108#puste#Untitled
#109#Nazwa:#Name:
#110#Szerokość:#Width:
#111#Wysokość:#Height:
#112#Gradienty#Gradients
#113#Krycie#Opacity
#114#Warstwy#Layers
#115#Zwykłe#Normal
#116#Krycie#Opacity
#117#Graf#Graph
#118#Szerokość:#Width:
#119#Wysokość:#Height:
#120#Zakres#Range
#121#Nacisk pióra#Pen pressure
#122#Kierunek pióra#Pen direction
#123#Pochylenie pióra#Pen tilt
#124#Obrót pióra#Pen rotation
#125#Kierunek rysowania#Stroke direction
#126#Wpływ prowadnic#Link to guides
#127#Prędkość rysowania #Stroke speed
#128#Losowo#Random
#129#Przejście#Transition
#130#Skala#Scale
#131#Zmiana po narysow.#Post correction
#132#Cofnij#Undo
#133#Przywróć#Redo
#134#Kopiuj#Copy
#135#Wklej#Paste
#136#Wytnij#Cut
#137#Nowy#New
#138#Otwórz#Open
#139#Zapisz#Save
#140#Zapisz jako#Save As
#141#Zdefiniuj skróty kl.#Define Hotkeys
#142#Opcje#Options
#143#Wyjście#Exit
#144#Balans koloru i jasność#Color balance, brightness
#145#Odcień i saturacja#Hue and Saturation
#146#Zmiana wielkości tła#Canvas resize
#147#Zmiana wielkości obrazu#Image resize
#148#Transformacja#Free transform
#149#Nowa warstwa#New Layer
#150#Nowy folder#New Folder
#151#Nowa maska Warstwy#New Mask Layer
#152#Usuń warstwę#Delete Layer
#153#Powiel warstwę#Duplicate layer
#154#Połącz warstwę w dół#Merge Layer Down
#155#Pomoc#Help
#156#O programie#About
#157#Włącz prowadnice#Enable guides
#158#Przyciągaj do prowadnic#Snap to guides
#159#Sprawdź aktualizacje#Check for updates
#160#Wyczyść#Clear
#161#Odwróć#Invert
#162#Wybierz wszystko#Select All
#163#Schowaj#Hide Selection
#164#Użyj aktualnej warstwy do selekcji#Use Reference Layer
#165#Aut. chowanie selekcji#Auto hide selection
#166#Resetuj widok#Reset View
#167#Obraz lustrzany#Flip View
#168#Przełącznik palet#Panels enabler/disabler
#169#Narzędzia#Tools
#170#Opcje narzędzia#Tool options
#171#Opcje pędzla#Brush options
#172#Warstwa#Layer
#173#Koło barw#Color wheel
#174#Nawigator#Navigator
#175#Wersje grafów #Graphs presets
#176#Paleta malarska#Mixer
#177#Pędzle#Brushes
#178#Próbki koloru#Color swatches
#179#Referencje#Reference
#180#Własna paleta 1#Custom 1
#181#Własna paleta 2#Custom 2
#182#Własna paleta 3#Custom 3
#183#1 wersja interfejsu#Workspace 1
#184#2 wersja interfejsu#Workspace 2
#185#3 wersja interfejsu#Workspace 3
#186#4 wersja interfejsu#Workspace 4
#187#5 wersja interfejsu#Workspace 5
#188#6 wersja interfejsu#Workspace 6
#189#Skróty klawiaturowe 1#Keyboard preset 1
#190#Skróty klawiaturowe 2#Keyboard preset 2
#191#Skróty klawiaturowe 3#Keyboard preset 3
#192#Schowaj palety#Hide panels
#193#Nawigator#Navigator
#194#Skala#Scale
#195#Kąt#Angle
#196# Sprawdź dostępność nowej wersji # Check what's new and download now
#197# Przypomnij później# Remind me later
#198#Paleta malarska#Mixer
#199#Dwuliniowe filtr. teks. (starsze karty graf.)#Bilinear tex. (For older graphic cards)
#200#Powiększ pod kursorem#Zoom on cursor
#201#Pokaż kursor pędzla#Show brush cursor
#202# wyczyść kolory#Reset colors
#203#Wyostrzenie obrazu przy oddalaniu#Canvas sharpness (zoom out)
#204#Wielkość elementów interfejsu#Global scale
#205#Szybkość powiększania#Zoom speed
#206#Szybkość zmiany wielkości pędzla#Brush resize speed
#207#Pikselacja przy powiększaniu#Pixelate on zoomin
#208#Próbki#Swatches
#209# Mnożenie# Multiply
#210# Odejmowanie# Substract
#211# Wysokość# Height
#212#Narzędzia#Tools
#213#  Plik#  File
#214#  Edycja#  Edit
#215#  Obraz#  Image
#216#  Warstwy#  Layer
#217#  Selekcja#  Selection
#218#  Widok#  View
#219#  Inne#  Other
#220#Zamknij szpary#Close gaps
#221#Wpływ szpar#Gaps effect
#222#Poszerz#inc
#223#Zakres#Tolerance
#224#Aktualna warstwa#Ref. layer
#225#Wszystkie warstwy#All layers
#226#Tylko aktywna warstwa#Pick from current layer
#227#Pokaż kolor przy próbniku#Show color bar near picker
#228#Zwykły#Normal
#229#Przeźroczystość#Opacity(pannel)
#229#Przeźroczystość#Opacity(pannel)
#230#Ignoruj TAB#Ignore TAB
#231#Zwykła przeźroczystość #Normal opacity on select
#232#Zwykła skala #Normal scale on select
#233#Skróty:#Hotkeys:
#234#Kolory:#Colors:
#235#Tryb mieszania:#Blend mode:
#236#Uwzględniaj kolory z dołu#Take underlayers color
#237#Take under#Take under
#238#Tryb tekstury:#Texture mode:
#239#Mnożenie#Multply
#240#Odejmowanie#Substract
#241#Wysokość#Height
#242#Własne#Custom
#243#Gradient czworokątny#Quad gradient
#244#Lekki gradient#Light gradient
#245#Multi-mieszanie#Multi blend
#246#Nacisk#Pressure
#247#Kształty koliste#Circle form
#248#Zanikanie#Fades
#249#Własne#Custom
#250#Ten gradient używa podstawowego and drugiego koloru#This gradient use MAIN and SECOND colors
#251#Ten gradient używa podstawowego koloru i krycia#This gradient use MAIN color and opacity
#252#Cienie#Shadows
#253#Półcienie#Midtones
#254#Światła#Highlights
#255#Resetuj skalę#Reset scale
#256#Resetuj kolory#Reset colors
#257#Wczytaj  główny pędzel#Load default brush
#258#Zapisz jako główny pędzel#Save as default brush
#259#Pojedyńczo:#Single forms:
#260#Zestaw:#Set of forms:
#261#OK (pojedyńczo)#OK (Single form)
#262#OK (zestaw)#OK (set of forms)
#263#Nowa wersja!#NEW VERSION!
#264#Dostępna nowa  wersja Paintstorm Studio#New version of Paintstorm Studio is now available
#265#Los. punktów#Rand points
#266#Los. linii#Rand stroke
#267#Sekwencja#Sequence
#268#Sk. losowości#Rand scale
#269#Sk. nacisku#Pressure scale
#270#Sk. prędkości#Speed scale
#271#skala pion. przy silnym nacisku#Vertical scale on max pressure
#272#Zapisz #Save 
#273#Selekcja!#Selection!
#274#Wyzeruj tło #Reset canvas angle
#275#Zmiana Pędzla przez przeciągnięcie#Brush resize by drag tool
#276#Zamień kolory#Swap main and second color
#277#Próbnik kolorów#Color picker tool
#278#Napełniaj pędzel podczas rysowania#Fill brush while drawing
#279#Selekcja prostokątna#Select rect tool
#280#Selekcja lasso#Lasso tool
#281#Wypełnianie#Fill tool
#282#Gradient#Gradient tool
#283#Przybliżanie#Zoom tool
#284#Przesuwanie#Hand tool
#285#Nowy dokument#New document
#286#Otwórz plik#Open file
#287#Odcień i nasycenie#Hue and saturation
#288#Balans koloru#Color balance
#289#Duplikuj warstwę#Duplicate layer
#290#Nowa warstwa#New layer
#291#Usuń warstwę#Delete layer
#292#Przyciągaj do prowadnic#Snap to guides
#293#Wybierz uśredniony kolor#Pick area color
#294#Szybki wybór warstwy#Fast layer selection
#295#Wypełnij#Fill all
#296#Usuń z selekcji#Delete from selection
#297#Skróty klawiaturowe 1#Keyboard preset1
#298#Skróty klawiaturowe 2#Keyboard preset2
#299#Skróty klawiaturowe 3#Keyboard preset3
#300#Szybkie wyłączenie kontroli pędzla#Fast disable brush controler
#301#Zapisz jako#Save As
#302#Zapisz#Save
#303#Wybierz drugi kolor#Pick second color
#304#Przełącz prowadnice#Enable-disable guides
#305#Przełącz tablet na cały ekran#Swap tablet to fullscreen
#306#Obróć obraz#Rotate canvas
#307#Usuń selekcję#Disable Selection
#308#automatyczna selekcja#Magic wand tool
#309#Przesuń obraz#Move canvas
#310#Przybliż#Zoom In
#311#Oddal#Zoom out
#312#Zmniejsz pędzel#Brush size +
#313#Zwiększ pędzel#Brush size -
#314#Cofnij#Undo
#315#Szybkie przybliżenie#Fast zoom tool
#316#Przywróć#Redo
#317#Transformacja#Free transform
#318#Przesuwanie warstwy#Move layer tool
#319#Pędzel#Brush tool
#320#Szybki próbnik#Fast Color picker
#321#Szybki podgląd#Fast preview
#322#Złącz dwie warstwy #Collaps two layers
#323#Kopiuj#Copy
#324#Wklej#Paste
#325#Odwróć zaznaczenie#Invert selection
#326#Wytnij#Cut
#327#Zaznacz Wszystko#Select all
#328#Gumka#Eraser tool
#329#1 wersja interfejsu#Workspace 1
#330#2 wersja interfejsu#Workspace 2
#331#3 wersja interfejsu#Workspace 3
#332#4 wersja interfejsu#Workspace 4
#333#5 wersja interfejsu#Workspace 5
#334#6 wersja interfejsu#Workspace 6
#335#Schowaj selekcję#Hide selection
#336#Odbij obraz#Reverse canvas
#337#Zmieść w ekranie#Fit to screen
#338#Wyświetl 1:1#Fit to normal scale
#339#Proste#Straight lines
#340#Poprzednia szczotki#Previous brush
#341#Przejrzyste linie#Guides opacity
#342#Ilość luster#Number of mirrors
#343#Powielanie#Duplication
#344#Symetria#Symmetry
#345#Zmniejsz duplikat#Reduce duplicates size
#346#   :działa do centrum#   :acts to center
#347#Dołącz symetrię#Toggle mirror
#348#Tryb gumki#Toggle eraser
#349#Chroń krycie#Preserve layer opacity
#350#Przełącznik clipping mask#Toggle clipping mask
#351#Lina#Rope
#352#Wiosna#Spring
#353#Narzędzie Kadrowanie#Crop
#354#Wybierz język#Choose Language
#355#Ustawienia Tablet#Tablet settings
#356#Zresetuj suwaki#Reset sliders keys
#357#Zablokuj warstwę#Lock layer
#359#   Zdefiniuj –#   Define –
#360#   Zdefiniuj +#   Define +
#361#Umożliwiać 1,2,3..9#Enable 1,2,3..9
#362#Reset#Reset
#363#Prędkość#Speed
#364#Klawiszy dla  #Define keys for  
#365#Pokaż / ukryj bieżącą warstwę#Show/hide current layer
#366#X Offset#X Offset
#367#Y Offset#Y Offset
#368#Width Offset#Width Offset
#369#Height Offset#Height Offset
#370#Koordynowanie tryb myszy#Coordinate mouse mode
#371#Włącz gumki na pióra revert#Enable pen-revert eraser
#372#Szybkie kolor odbiór#Fast pick color
#373#Wielkość siatki#Grid spacing
#374#-Czysta lista-#-Clear List-
#375#Ostatnie pliki#Recent Files
#376#Rozmiar miniatur#Thumbnail size
#377#Przywróć domyślne#Reset to default
#378#Obciążenia UI#Load custom UI
#379#Zapisz zwyczaj UI#Save custom UI
#380#Resetowanie Wszystkie#Reset All
#381#Resetowanie układ#Reset panels layout
#382#Resetowanie kolory#Reset panels colors
#383#Interfejs reset kolorów#Reset interface color
#384#Reset niestandard. panelach #Reset custom pannels
#385#Resetowanie próbki#Reset swatches
#386#Resetowanie hotkeys#Reset hotkeys
#387#Tworzenie nowych gradientu#Create new gradient
#388#Usuń bieżące gradientu#Delete current gradient
#389#Importuj gradienty#Import gradients
#390#Eksport gradienty#Export gradients
#391#Resetowanie gradienty#Reset gradients
#392#Włącz paski przewijania#Enable scroll bars
#393#Utwórz nową kategorię#Create New Category
#394#Usuń aktualnej kategorii#Remove Current Category
#395#Zmień kategorię Ikona#Change Category Icon
#396#Zmień nazwę Kategoria#Rename Category
#397#Kategoria Move UP#Category Move UP
#398#Kategoria Move DOWN#Category Move DOWN
#399#Przenieś Kategoria Lista Przesuń w lewo#Move Category List To LEFT
#400#Przenieś Kategoria Lista Na górę#Move Category List On TOP
#401#Importuj Szczotki#Import Brushes
#402#Eksport Szczotki#Export Brushes
#403#Importuj ABR#Import ABR
#404#Resetowanie aktualnej kategorii#Reset Current Category
#405#Resetowanie Wszystkie Szczotki Pędzle#Reset All Brushes
#406#Importuj nowy formularz#Import new form
#407#Tworzenie nowego folderu#Create new folder
#408#Usuń formularz aktualny#Delete current form
#409#Usuń bieżącym folderze#Delete current folder
#410#formy Import z ABR form#Import forms from ABR
#411#Tworzenie nowego presetu#Create new preset
#412#Importuj nowe tekstury#Import new texture
#413#Usuń bieżące ustawienie#Delete current preset
#414#Importuj Nowy Ikona#Import new Icon
#415#Usuń bieżące tekstury#Delete current texture
#416#Usuń ikonę bieżącego#Delete current icon
#417#-Tworzenie nowa kategoria-#-Create new category-
#418#Zastosuj do aktualnych pędzla#Apply to current brush
#419#Wprowadź nazwę:#Enter name:
#420#Capture Form#Capture Form
#421#Normalizować#Normalize
#422#Maska#Mask
#423#Synchronizacja#Syncronization
#424#Przemianować#Rename
#425#Wpływać he radialny gradient#Affect on radial gradient
#426#Scalanie dolne warstwy w jeden nowy#Merge lower layers into one new
#427#Linijka#Ruler
#428#Elipsa#Ellipse
#429#2-point#2-point
#430#2-point perspective#2-point perspective
#431#3-point#3-point
#432#3-point perspective#3-point perspective
#433#Twardość ciśnienia Globalny#Global pressure hardness
#434#Zastosuj maskę#Apply mask
#435#Wybierz warstwę#Select layer
#436#Rozmyte#Blur
#437#Miniatury z granic warstw#Thumbnails with Layer bounds
#438#Pokaż szczotka krąg#Show brush circle
#439#Scenariusz#Script
#440#Postęp#Progress
#441#Szybkość odtwarzania#Play speed
#442#Stroke#Stroke
#443#Zmień Brush#Change Brush
#444#Ostatnia akcja:#Last Action:
#445#Następny akcja:#Next Action:
#446#Zmiana opcji #Change option 
#447#Resetowanie ustawień pędzla, aby rozpocząć#Reset brush to start settings
#448#W stosunku do wielkości płótnie#Relative to canvas size
#449#Rozmiar pędzla#Brush size
#450#Pamiętaj losowo#Remember randoms
#451#Przerwa na resecie szczotki#Pause on brush reset
#452#Utwórz nowe płótno na początku#Create new canvas on start
#453#Przegubowe władcy#Toggle rulers
#454#Clone#Clone
#455#Wyłącz prostokąta#Disable rectangle
#456#  Filtry#Filters
#457#Plama#Blur
#458#Siła#Strength
#459#Zapowiedź#Preview
#460#Wyostrzyć#Sharpen
#461#Rozkład:#Resolution:
#462#Pikseli#Pixels
#463#Cale#Inches
#464#Milimetrów#Millimeters
#465#Gama kolorystyczna#Color range
#466#Twardość#Hardness
#467#Bieżąca warstwa#Current layer
#468#Odwróć poziomo#Flip horizontal
#469#Przerzuć w pionie#Flip Vertical
#470#Osnowa#Warp
#471#100#Full
#472#50#Half
#473#25#Quarter
#474#15#Draft
#475#Resetowanie Siatka#Reset Grid
#476#Pokaż Pod warstwami#Show Under Layers
#477#Pokaż górnych warstwach#Show Upper Layers
#478#Proszę wybrać aktywny obszar#Please choose active area and press OK
#479#Pasować do:#Fit To:
#480#Blokowanie krawędzi#Pin Edges
#481#Nie można wyciągnąć, gdy wybrany jest więcej niż jedna warstwa#You can not draw when more than one layer is selected
#482#Pokaż / ukryj warstwę odniesienia#Show/hide reference layer
#483#Szybki ruch warstwy#Fast layer move
#484#Tryb Jednolite#Seamless mode
#485#Pokaż ramki#Show frame
#486#Temperatura#Temperature
#487#Odcień#Tint
#488#Tryb wykresów:#Graph mode
#489#Klasyczny#Classic
#490#Proste#Straight
#491#Zachowaj jasność#Preserve Luminosity
#492#Wybrane warstwy#Selected layers
#493#Inteligentna korekcja kolorów#Smart Color Correction
#494#Stosuje się do:#Apply to:
#495#Dodaj nową maskę koloru#Add new color mask
#496#Włącz maskę odcienia#Enable Hue mask
#497#Włącz maskę nasycenia#Enable Saturation mask
#498#Włącz maskę jasność#Enable Luminosity mask
#499#Usunąć#Remove
#500#Ustaw kolor#Set color
#501#Usuń z pamięcią#Delete from presets
#502#Zaawansowane opcje#Advanced options
#503#nasycenie wyjście#Output Saturation
#504#Kontrast nasycenia#Output Saturation Contrast
#505#Kanał#Channel
#506#Import#Import
#507#Eksport#Export
#508#Zamknij dokument#Close current document
#509#Tryb klonu:#Clone mode:
#510#Główny#Main
#511#Berło#Interface
#512#Wydajność#Performance
#513#Kursory#Cursors
#514#OpenGL#OpenGL
#515#Przyspieszenie GPU#GPU Acceleration
#516#GPU dla szczotek szczotkowych (OpenGL 4.3)#GPU for bristle brushes (OpenGL 4.3)
#517#Automatyczne wyłączenie GPU dla małych szczotek#Auto disable GPU for small brushes
#518#Zawsze pokazuj kontur pędzla jako okrąg#Always show brush outline as circle
#519#Na rysunku:#On Drawing:
#520#Undos liczyć#History states
#521#Pamięć historii#History memory
#522#Maksymalna ilość szczotek#Maximum amount of brushes
#523#Włącz duże płótna (nie zalecane)#Enable large canvases (Not recommended)
#524#(Potrzeba restartu, aby zacząć działać)#(Need restart to take effect)
#525#Włącz eksperymentalny tryb GPU#Enable experimental GPU fast mode
#526#Raz po jednym#One by one
#527#Zresetuj kolor szczotki#Reset brush color
#528#Konwersja szczotek. Proszę czekać...#Converting brushes. Please wait...
#529#Użyj suwaków gradientu#Use gradient sliders
#530#Wyczyść warstwę#Clear layer
#531#Wyłącz transformację#Disable Transformation
#532#Max lustra#Max mirrors
#533#Maks. Duplikacja luster#Max duplications for mirrors
#534#Przesuń warstwę powyżej#Move layer above
#535#Przesuń warstwę poniżej#Move layer below