//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;

uniform sampler1D gradTexture;

uniform float width;
uniform float height;
uniform vec4 color;
uniform vec4 color0;
uniform vec4 color1;
uniform bool vertFl;
uniform bool gradFl;
uniform float vertGrad;

///////////////////////////////
///////////////////////////////////////
vec4 getGradColor(float gip) {


	if (gip > 1) { gip = 1; }

	float num = 0;
	num = texture(gradTexture, 0).r;


	float li = 0;

	for (int i = 0; i < num; i++) {
		if (texture(gradTexture, float(i + 1) / 256).g > gip) {
			break;
		}
		li = i;
	}

	float posl = texture(gradTexture, (li + 1) / 256).g;
	float posr = texture(gradTexture, (li + 2) / 256).g;

	vec4 coll = texture(gradTexture, (li + 1 + num) / 256);
	vec4 colr = texture(gradTexture, (li + 2 + num) / 256);

	float dec = gip - posl;

	if (dec > 0) {
		dec /= posr - posl;
	}
	else {
		dec = 0;
	}

	if (dec < 0) { dec = 0; }
	if (dec > 1) { dec = 1; }

	vec4 res = coll*(1 - dec) + colr*(dec);



	return res;
}
//////////////////////////////
void main() {

		FragColor = color;
		

		vec2 cor = vec2(texCoords.x*width, texCoords.y*height);

		float rad = height / 2;
		float dist = 0;
		//-------------------------------------

		
		if (vertFl) {
			rad = width / 2;

			if (cor.y <= rad) {
				dist = distance(cor, vec2(rad, rad));
			}

			if (cor.y >= height - rad) {
				dist = distance(cor, vec2(rad, height - rad));
			}
		}
		else {

			if (cor.x <= rad) {
				dist = distance(cor, vec2(rad, rad));
			}

			if (cor.x >= width - rad) {
				dist = distance(cor, vec2(width - rad, rad));
			}
		}
		//------------------------------------
			if (dist > rad+0.5) {
				FragColor.a = 0;
				return;
			}

			if (rad+0.5 - dist < 1) {
				FragColor.a *= rad+0.5-dist;
			}
		//---------------------------------

			if (gradFl && !vertFl) {

				float a = FragColor.a;
				FragColor = getGradColor(texCoords.x);
				FragColor.a *= a;
			
			}
			//---------
			
			if (vertGrad != 0) {
				float dy = texCoords.y;
				float a = FragColor.a;
				FragColor = FragColor*dy + FragColor*vertGrad*(1 - dy);
				FragColor.a = a;
			}

			
}