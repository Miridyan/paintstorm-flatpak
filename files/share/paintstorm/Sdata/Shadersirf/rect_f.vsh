//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;

uniform float width;
uniform float height;
uniform float rad;
uniform bool botFl;
uniform vec4 color;

uniform vec4 secondColor;
uniform bool gradFl;

uniform bool frameFl;
uniform bool frameOnlyFl;
uniform vec4 frameColor;

void main() {

		FragColor = color;

		//----------------------
		if (gradFl) {

			float dist = 1 - texCoords.y;

			FragColor = color*dist + secondColor*(1 - dist);
			
		}
		//----------------------
		

		vec2 cor = vec2(texCoords.x*width, texCoords.y*height);

		float dist = 0;
		bool fl = false;

		if (rad != 0) {
			//-------------------------------------
			if (cor.x <= rad && cor.y <= rad) {
				dist = distance(cor, vec2(rad, rad));
				fl = true;
			}

			if (cor.x >= width - rad && cor.y <= rad) {
				dist = distance(cor, vec2(width - rad, rad));
				fl = true;
			}

			if (botFl) {
				if (cor.x >= width - rad && cor.y >= height - rad) {
					dist = distance(cor, vec2(width - rad, height - rad));
					fl = true;
				}

				if (cor.x <= rad && cor.y >= height - rad) {
					dist = distance(cor, vec2(rad, height - rad));
					fl = true;
				}
			}
			//------------------------------------

			if (dist > rad + 0.5) {
				FragColor.a = 0;
				return;
			}

			if (rad + 0.5 - dist < 1) {
				FragColor.a *= rad + 0.5 - dist;
			}

			//-------------------------
		}
			if (frameFl) {
				if (frameOnlyFl) {
					FragColor = frameColor;
				}
				vec4 col = frameColor;

				bool flz = false;

				if (rad - 0.8 - dist < 1) {
					float a = 1 - abs(rad - 0.8 - dist) / 0.8;
					FragColor.r = FragColor.r*(1 - a) + col.r*a;
					FragColor.g = FragColor.g*(1 - a) + col.g*a;
					FragColor.b = FragColor.b*(1 - a) + col.b*a;

					if (frameOnlyFl) {
						FragColor.a = a;
					}
					flz = true;
				}
				if (rad - 0.8 - dist < 0.0) {

					FragColor.r = col.r;
					FragColor.g = col.g;
					FragColor.b = col.b;
					flz = true;
				}
			

				//------------
				if (!fl) {
					if (cor.x <= 1 || cor.y <= 1 || cor.x >= width - 1 || cor.y >= height - 1) {
						FragColor = col;
						flz = true;
					}
				}

				if (!flz && frameOnlyFl) {
					FragColor.a = 0;
				}
			}
}