//Copyright (C) 2014-2017 Paintstorm Studio
#version 150

in vec2 texCoords;
out vec4 FragColor;


uniform sampler2D formTexture;
uniform sampler2D mTexture;
uniform sampler2D layTexture;
uniform sampler2D underTexture;
uniform sampler2D selectTexture;

uniform bool dcd_select;

uniform float XX;
uniform float YY;

uniform float texsize;
uniform int border;
uniform int size;

uniform int vx;
uniform int vy;
uniform int vw;
uniform int vh;


uniform float texposx;
uniform float texposy;
uniform float docc_width;
uniform float docc_height;
//-----------brush-------------
uniform bool brush_pickunder;
uniform float brush_size;
uniform int brush_brmode;
//-----------------------------
uniform float cosm;
uniform float sinm;
uniform float brush_squeezex;
uniform float brush_squeezey;

uniform bool blur_enable;

void main() {



	//-----------
	float alpha;

	if (!blur_enable) {

		vec2 rot = (vec2(texCoords.x*vw, texCoords.y*vh) + vec2(vx, vy) - vec2(border, border)) / brush_size;

		if (brush_brmode == 2) {
			float ka = 0.5 - rot.x;
			float kb = 0.5 - rot.y;

			rot.x = ka*cosm - kb*sinm;
			rot.y = ka*sinm + kb*cosm;

			rot.x *= brush_squeezex;
			rot.y *= brush_squeezey;

			rot.x += 0.5;
			rot.y += 0.5;
		}


		alpha = texture2D(formTexture, rot).r;

		if (alpha == 0) {
			FragColor.a = 0;
			return;
		}
	}
	else {
		alpha = 1;
	}
	//-------------



	vec2 cor = vec2(texCoords.x*vw, texCoords.y*vh) + vec2(XX, YY);


	if (cor.x + texposx >= docc_width || cor.y + texposy >= docc_height) {
		FragColor = vec4(0, 0, 0, 0);
		return;
	}
	if (cor.x + texposx < 0 || cor.y + texposy < 0) {
		FragColor = vec4(0, 0, 0, 0);
		return;
	}

	cor /= texsize;


	vec4 col;

	if (brush_pickunder) {
		
		vec4 colya = texture2D(layTexture, cor);
		vec4 colxa = texture2D(mTexture, cor);

	
		if (colya.a < 1) {
			col = texture2D(underTexture, cor);
			col = colya *colya.a + col*(1 - colya.a);
		}
		else {
			col = colya;
		}
		
		col = col*(1 - colxa.a) + colxa*colxa.a;

		if (col.a == 0) {
			FragColor = vec4(0, 0, 0, 0);
			return;
		}

		col.a = 1;
	}
	else {

		col = vec4(0, 0, 0, 0);
		vec4 colya = texture2D(layTexture, cor);
		vec4 colxa = texture2D(mTexture, cor);

		float xa = colxa.a;
		float ya = colya.a;
		float ta = xa + ya - ya*xa;
		if (ta != 0) {
			float dec1 = ya*(1 - xa) / ta;
			float dec2 = xa / ta;
			col = colya*dec1 + colxa*dec2;
			col.a = ta;
		}

		if (col.a == 0) {
			FragColor = vec4(0, 0, 0, 0);
			return;
		}

	}

	col.a *= alpha;

	if (dcd_select) {
		col.a *= texture2D(selectTexture, cor).r;
	}


	FragColor = col;
		
	
	
}

