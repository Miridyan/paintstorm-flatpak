//Copyright (C) 2014-2017 Paintstorm Studio
#version 430


layout(local_size_x = 32) in;

uniform sampler2D mtexture0;
uniform sampler2D mtexture1;
uniform sampler2D mtexture2;
uniform sampler2D mtexture3;

uniform sampler2D laytexture0;
uniform sampler2D laytexture1;
uniform sampler2D laytexture2;
uniform sampler2D laytexture3;

uniform sampler2D undertexture0;
uniform sampler2D undertexture1;
uniform sampler2D undertexture2;
uniform sampler2D undertexture3;




uniform float XX;
uniform float YY;

layout(rgba16)  uniform image2D _mTexture0;
layout(rgba16)  uniform image2D _mTexture1;
layout(rgba16)  uniform image2D _mTexture2;
layout(rgba16)  uniform image2D _mTexture3;

layout(r32f)  uniform image2D vors_texture;

uniform int vors_group_count;

uniform float sz;
uniform float texsize;
uniform int border;


uniform int vors_stretch;//stretch
uniform int vors_tsize;
uniform int vors_wid;

uniform int vors_layer_offset;


//-----brush-----

uniform bool brush_pickunder;
uniform vec4 brush_curcolor;



//----------
uniform float squeezex;
uniform float squeezey;
uniform float cosm;
uniform float sinm;
uniform float cxrat;


uniform int tex_offset_x;
uniform int tex_offset_y;
uniform int docc_width;
uniform int docc_height;



///////////////////////////////////////////////////
struct _vors{

	//int curn;
	//int finn;
	//float dxf;
	//float dyf;

	int grN;

	float alfa;
	float x;
	float y;
	float oldx;
	float oldy;
	bool halen;
	vec4 colrgba;// r, g, b, a;
	vec4 colrigibiai;// ri, gi, bi, ai;

};

/////////////////////////////////////////
vec4 getPixelFromLaytexture(int px, int py) {
	if (px < texsize && py < texsize) {
		return texture(laytexture0, vec2(px, py) / texsize);
	}
	if (px >= texsize && py < texsize) {
		return texture(laytexture1, vec2(px - texsize, py) / texsize);
	}
	if (px >= texsize && py >= texsize) {
		return texture(laytexture2, vec2(px - texsize, py - texsize) / texsize);
	}
	if (px < texsize && py >= texsize) {
		return texture(laytexture3, vec2(px, py - texsize) / texsize);
	}
}
/////////////////////////////////////////
vec4 getPixelFromUndertexture(int px, int py) {
	if (px < texsize && py < texsize) {
		return texture(undertexture0, vec2(px, py) / texsize);
	}
	if (px >= texsize && py < texsize) {
		return texture(undertexture1, vec2(px - texsize, py) / texsize);
	}
	if (px >= texsize && py >= texsize) {
		return texture(undertexture2, vec2(px - texsize, py - texsize) / texsize);
	}
	if (px < texsize && py >= texsize) {
		return texture(undertexture3, vec2(px, py - texsize) / texsize);
	}
}
/////////////////////////////////////////
vec4 getPixelFromMtexture(int px, int py) {
	if (px < texsize && py < texsize) {
		return texture(mtexture0, vec2(px, py) / texsize);
	}
	if (px >= texsize && py < texsize) {
		return texture(mtexture1, vec2(px-texsize, py) / texsize);
	}
	if (px >= texsize && py >= texsize) {
		return texture(mtexture2, vec2(px-texsize, py-texsize) / texsize);
	}
	if (px < texsize && py >= texsize) {
		return texture(mtexture3, vec2(px, py-texsize) / texsize);
	}
}
/////////////////////////////////////////
void storePixelToMtexture(int px, int py, vec4 col) {
	if (px < texsize && py < texsize) {
		imageStore(_mTexture0, ivec2(px, py), col);
		return;
	}
	if (px >= texsize && py < texsize) {
		imageStore(_mTexture1, ivec2(px-texsize, py), col);
		return;
	}
	if (px >= texsize && py >= texsize) {
		imageStore(_mTexture2, ivec2(px-texsize, py-texsize), col);
		return;
	}
	if (px < texsize && py >= texsize) {
		imageStore(_mTexture3, ivec2(px, py-texsize), col);
		return;
	}
}
//////////////////////////////////////////
_vors vors;
float dxf, dyf;
float xa, ya, ta, dec1, dec2;
vec4 color;//rr
float tmpf = -1;
//======================================================
void main() {
	
	if (gl_GlobalInvocationID.x >= vors_group_count - vors_layer_offset) { return; }

	int idxy = int(gl_GlobalInvocationID.x + vors_layer_offset)/vors_wid;
	

	int idxx = (int(gl_GlobalInvocationID.x + vors_layer_offset) - idxy*vors_wid)*vors_stretch;
	

	

	//------------1st----------------------------------------------------------

		vors.x = imageLoad(vors_texture, ivec2(idxx + 6, idxy)).r;
		vors.y = imageLoad(vors_texture, ivec2(idxx + 7, idxy)).r;

		float xx = vors.x;
		float yy = vors.y;

		float ka = squeezex*(512 - xx);
		float kb = squeezey*(512 - yy);

		xx = XX + cxrat + ka*cosm - kb*sinm;
		yy = YY + cxrat + ka*sinm + kb*cosm;
		
	
	
	//--------------------------------------------------------------

	

	float x = xx;
	float y = yy;
	
	//////////--------------------grN-----------------------------------------
	
	
	//----------------------------------------------------------------

	int px = int(x) - tex_offset_x;
	int py = int(y) - tex_offset_y;

	if (x < 0 || y < 0 || x >= docc_width - 1 || y >= docc_height - 1) { return; }
	//---------------------------------------------------------------


	//---------------------------------------------------------------
	//-----------------------------color-----------------------------
	//---------------------------------------------------------------


	color = getPixelFromLaytexture(px,py);
		ya = color.a;
	
	//*****************
	if (brush_pickunder) {

		vec4 undercolor = getPixelFromUndertexture(px, py);
		color = color*ya + undercolor*(1 - ya);
		
		vec4 ldataf= getPixelFromMtexture(px, py);
		ya = ldataf.a;

		color = color*(1 - ya) + ldataf*ya;
		color.a = 1;

	}
	else {
	//******************
		vec4 ldataf = getPixelFromMtexture(px, py);
		xa = ldataf.a;

		ta = xa + ya - xa*ya;

		if (ta != 0) {
			dec1 = ya*(1 - xa) / ta;
			dec2 = xa / ta;
			
			color = color*dec1 + ldataf*dec2;
		}

		color = color*ta + brush_curcolor*(1 - ta);
		color.a = ta;
	}
	

						
		imageStore(vors_texture, ivec2(idxx + 11, idxy), vec4(color.r));
		imageStore(vors_texture, ivec2(idxx + 12, idxy), vec4(color.g));
		imageStore(vors_texture, ivec2(idxx + 13, idxy), vec4(color.b));
		imageStore(vors_texture, ivec2(idxx + 14, idxy), vec4(1));
		
		imageStore(vors_texture, ivec2(idxx + 15, idxy), vec4(color.r));
		imageStore(vors_texture, ivec2(idxx + 16, idxy), vec4(color.g));
		imageStore(vors_texture, ivec2(idxx + 17, idxy), vec4(color.b));
		imageStore(vors_texture, ivec2(idxx + 18, idxy), vec4(1));

	
}