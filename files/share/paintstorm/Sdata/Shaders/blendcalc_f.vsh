//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;



uniform sampler2D blendTexture;

uniform int size;

uniform int flag;

uniform float maxbrushsize;

uniform int step;

uniform float alphasum;

uniform bool approx;
uniform vec4 maincolor;
//---brush---
uniform float brush_hale_cur;
uniform float brush_coloramount_cur;
uniform vec4 brush_curcolor;
uniform bool brush_pickunder;
uniform bool brush_dirty;
//----------


void main() {
	

	//-----------------------quad steps-----------	
	if (flag == 0) {

	
		
		ivec2 quad = ivec2(texCoords * size)*step;

		vec4 sumcol = vec4(0, 0, 0, 0);
		float numofpoints = 0;
		float numofpointsA = 0;

		int szq = size*step-step;

		int st;

		if (approx) { 
			st = 1; 
		}
		else {
			st = step;
		}
	

		for (int j = quad.y; j < quad.y + st; j++) {
			for (int i = quad.x; i < quad.x + st; i++) {

				

				if (i > szq || j > szq) { continue; }
				
				
				vec4 col = texture2D(blendTexture, vec2(i, j) / maxbrushsize);

				if (col.a == 0) { 
					numofpoints += 1;
					continue; 
				}

				numofpointsA += col.a;
				sumcol.r += col.r*col.a;
				sumcol.g += col.g*col.a;
				sumcol.b += col.b*col.a;
				sumcol.a += col.a;
				numofpoints += 1;

			}
		}

		if (numofpointsA != 0) {
			sumcol.r /= numofpointsA;
			sumcol.g /= numofpointsA;
			sumcol.b /= numofpointsA;
			sumcol.a /= numofpoints;
		}

		FragColor = sumcol;

		return;
	}
	
	//============================calc blend================

	if (flag == 1) {

		vec4 sumcol = vec4(0, 0, 0, 0);
		float numofpoints = 0;
		float numofpointsA = 0;


		for (int j = 0; j < size; j++) {
			for (int i = 0; i < size; i++) {


				vec4 col = texture2D(blendTexture, vec2(i, j) / maxbrushsize);


				if (col.a == 0) { 
					numofpoints += 1;
					continue; 
				}

				numofpointsA += col.a;
				sumcol.r += col.r*col.a;
				sumcol.g += col.g*col.a;
				sumcol.b += col.b*col.a;
				sumcol.a += col.a;
				numofpoints += 1;

			}
		}

		if (numofpointsA != 0) {
			sumcol.r /= numofpointsA;
			sumcol.g /= numofpointsA;
			sumcol.b /= numofpointsA;
			sumcol.a /= numofpoints;
		}

		FragColor = sumcol;

		return;
	}


	//================================= hale color ====================

	if (flag == 2) {
		vec4 col = texture2D(blendTexture, vec2(0,1023)/1024); //cur blend col
		vec4 brush_hale_halecolor = texture2D(blendTexture, vec2(1, 1023) / 1024);//prev hale color
		

		//-----F-------------
		if (maincolor.r != -1) {
			col = maincolor;
			brush_hale_halecolor = maincolor;
		}
		//--------------

		bool brush_hale_halen = true;
	
		if (brush_hale_halecolor.a < 0.01) {
			if (brush_hale_halecolor.r == 0) {
				brush_hale_halen = false;
			}
		}

		//-----------------
		if (col.a < 0.01) {

			if (brush_hale_halecolor.r == 0 && brush_hale_halecolor.g == 0 && brush_hale_halecolor.b == 0 && brush_hale_halecolor.a == 0) {
				FragColor = vec4(0, 0, 0, 0);//halen=false
			}
			else {
				FragColor = vec4(1, 0, 0, 0);//halen=true
			}

			return;
		}
		//---------
		if ( !brush_hale_halen) {
			if (brush_dirty) {
				vec4 brush_dirtycolor = texture2D(blendTexture, vec2(5, 1023) / 1024);//dirty color
				brush_hale_halecolor = brush_dirtycolor;
			}
		}

		if (brush_pickunder) {
			col.a = 1;
		}

		//---------
		if (brush_hale_halecolor.a > 0.01) {

			float tmpf = brush_hale_cur;
			col = col*tmpf + (1 - tmpf)*brush_hale_halecolor;
			
		}

	//	brush_hale_halecolor = col;
		//-------------
		
		FragColor = col;

		return;
	}

	//==========================result color==============
	if (flag == 3) {

		vec4 col = texture2D(blendTexture, vec2(1, 1023) / 1024);//halecolor

		if (!brush_pickunder) {
			col.a *= alphasum;
			if (col.a > 1) { col.a = 1; }
		}

		float tmpf = brush_coloramount_cur;

		if (col.a > 0.01) {
			col = tmpf*brush_curcolor + (1 - tmpf)*col;
		}
		else {
			col = brush_curcolor;
			col.a = tmpf;
		}

		FragColor =  col;

		return;
	}
	//========================================
	
}

