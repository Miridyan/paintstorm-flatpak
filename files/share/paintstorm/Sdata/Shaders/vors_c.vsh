//Copyright (C) 2014-2017 Paintstorm Studio
#version 430


layout(local_size_x = 32) in;

uniform sampler2D mtexture0;
uniform sampler2D mtexture1;
uniform sampler2D mtexture2;
uniform sampler2D mtexture3;

uniform sampler2D laytexture0;
uniform sampler2D laytexture1;
uniform sampler2D laytexture2;
uniform sampler2D laytexture3;

uniform sampler2D undertexture0;
uniform sampler2D undertexture1;
uniform sampler2D undertexture2;
uniform sampler2D undertexture3;

uniform sampler2D selecttexture0;
uniform sampler2D selecttexture1;
uniform sampler2D selecttexture2;
uniform sampler2D selecttexture3;

uniform sampler2D brtexTexture;

uniform float XX;
uniform float YY;

layout(rgba16)  uniform image2D _mTexture0;
layout(rgba16)  uniform image2D _mTexture1;
layout(rgba16)  uniform image2D _mTexture2;
layout(rgba16)  uniform image2D _mTexture3;

layout(r32f)  uniform image2D vors_texture;

uniform int vors_group_count;

uniform bool brokang;

uniform float sz;
uniform float texsize;


uniform bool dcd_select;

uniform int vors_stretch;//stretch
uniform int vors_tsize;
uniform int vors_wid;

uniform int vors_layer_offset;

uniform int jj;
//-----brush-----
uniform float brush_coloramount_cur;
uniform float brush_hale_cur;
uniform bool brush_pickunder;
uniform vec4 brush_curcolor;
uniform vec4 maincolor;
uniform float brush_transparency_cur;

uniform bool brush_enabletexture;
uniform int brush_texture_size;
uniform float brush_texscale_cur;
uniform float brush_texoffsetx;
uniform float brush_texoffsety;
uniform bool brush_texinvert;
uniform int brush_texturemode;
uniform float brush_texeffect_cur;
uniform float brush_texcontrast_cur;

uniform float brush_dirtystrength_cur;
uniform bool brush_dirty;
uniform bool brush_preservedirty;
uniform float brush_bristb_cur;
uniform float brush_vorscount;
uniform int brush_grad_usesingle;
uniform float brush_grad_cur;
//----------
uniform bool pickarea;

uniform float squeezex;
uniform float squeezey;
uniform float cosm;
uniform float sinm;
uniform float cxrat;
uniform bool resetold;

uniform int tex_offset_x;
uniform int tex_offset_y;
uniform int docc_width;
uniform int docc_height;

////////////////////////////////////////
float maxrgb(vec4 col) {
	if (col.r >= col.g && col.r >= col.b) { 
		return col.r; 
	}
	else {
		if (col.g >= col.b) { return col.g; }
		else { return col.b; }
	}
}
////////////////////////////////////////
float contrast(float contrast, float ga) {
	float cc = contrast*0.5;
	float cc1 = 1 - cc;

	if (ga<cc) {
		ga -= (cc - ga);
		if (ga<0) { ga = 0; }
		return ga;
	}
	if (ga>cc1) {
		ga += (ga - cc1);
		if (ga>1) { ga = 1; }
	}
	return ga;
}
///////////////////////////////////////////////////
struct _vors{

	//int curn;
	//int finn;
	//float dxf;
	//float dyf;

	int grN;

	float alfa;
	float x;
	float y;
	float oldx;
	float oldy;
	bool halen;
	vec4 colrgba;// r, g, b, a;
	vec4 colrigibiai;// ri, gi, bi, ai;
	vec4 colrdgdbdad;// rd, gd, bd, ad;
};
/////////////////////////////////////////
float getPixelFromSelecttexture(int px, int py) {
	if (px < texsize && py < texsize) {
		return texture(selecttexture0, vec2(px, py) / texsize).r;
	}
	if (px >= texsize && py < texsize) {
		return texture(selecttexture1, vec2(px - texsize, py) / texsize).r;
	}
	if (px >= texsize && py >= texsize) {
		return texture(selecttexture2, vec2(px - texsize, py - texsize) / texsize).r;
	}
	if (px < texsize && py >= texsize) {
		return texture(selecttexture3, vec2(px, py - texsize) / texsize).r;
	}
}
/////////////////////////////////////////
vec4 getPixelFromLaytexture(int px, int py) {
	if (px < texsize && py < texsize) {
		return texture(laytexture0, vec2(px, py) / texsize);
	}
	if (px >= texsize && py < texsize) {
		return texture(laytexture1, vec2(px - texsize, py) / texsize);
	}
	if (px >= texsize && py >= texsize) {
		return texture(laytexture2, vec2(px - texsize, py - texsize) / texsize);
	}
	if (px < texsize && py >= texsize) {
		return texture(laytexture3, vec2(px, py - texsize) / texsize);
	}
}
/////////////////////////////////////////
vec4 getPixelFromUndertexture(int px, int py) {
	if (px < texsize && py < texsize) {
		return texture(undertexture0, vec2(px, py) / texsize);
	}
	if (px >= texsize && py < texsize) {
		return texture(undertexture1, vec2(px - texsize, py) / texsize);
	}
	if (px >= texsize && py >= texsize) {
		return texture(undertexture2, vec2(px - texsize, py - texsize) / texsize);
	}
	if (px < texsize && py >= texsize) {
		return texture(undertexture3, vec2(px, py - texsize) / texsize);
	}
}
/////////////////////////////////////////
vec4 getPixelFromMtexture(int px, int py) {
	if (px < texsize && py < texsize) {
		return texture(mtexture0, vec2(px, py) / texsize);
	}
	if (px >= texsize && py < texsize) {
		return texture(mtexture1, vec2(px-texsize, py) / texsize);
	}
	if (px >= texsize && py >= texsize) {
		return texture(mtexture2, vec2(px-texsize, py-texsize) / texsize);
	}
	if (px < texsize && py >= texsize) {
		return texture(mtexture3, vec2(px, py-texsize) / texsize);
	}
}
/////////////////////////////////////////
void storePixelToMtexture(int px, int py, vec4 col) {
	if (px < texsize && py < texsize) {
		imageStore(_mTexture0, ivec2(px, py), col);
		return;
	}
	if (px >= texsize && py < texsize) {
		imageStore(_mTexture1, ivec2(px-texsize, py), col);
		return;
	}
	if (px >= texsize && py >= texsize) {
		imageStore(_mTexture2, ivec2(px-texsize, py-texsize), col);
		return;
	}
	if (px < texsize && py >= texsize) {
		imageStore(_mTexture3, ivec2(px, py-texsize), col);
		return;
	}
}
//////////////////////////////////////////
_vors vors;
float dxf, dyf;
float xa, ya, ta, dec1, dec2;
vec4 color;//rr
float tmpf = -1;
//======================================================
void main() {
	
	if (gl_GlobalInvocationID.x >= vors_group_count - vors_layer_offset) { return; }

	int idxy = int(gl_GlobalInvocationID.x + vors_layer_offset)/vors_wid;
	

	int idxx = (int(gl_GlobalInvocationID.x + vors_layer_offset) - idxy*vors_wid)*vors_stretch;
	

	

	//------------1st----------------------------------------------------------
	if (jj == 0) { 
		vors.x = imageLoad(vors_texture, ivec2(idxx + 6, idxy)).r;
		vors.y = imageLoad(vors_texture, ivec2(idxx + 7, idxy)).r;

		float xx = vors.x;
		float yy = vors.y;

		float ka = squeezex*(512 - xx);
		float kb = squeezey*(512 - yy);

		xx = XX + cxrat + ka*cosm - kb*sinm;
		yy = YY + cxrat + ka*sinm + kb*cosm;
		

		vors.oldx = imageLoad(vors_texture, ivec2(idxx + 8, idxy)).r;
		vors.oldy = imageLoad(vors_texture, ivec2(idxx + 9, idxy)).r;

		if (resetold || vors.oldx == -333) {
			imageStore(vors_texture, ivec2(idxx + 8, idxy), vec4(xx));
			imageStore(vors_texture, ivec2(idxx + 9, idxy), vec4(yy));
			imageStore(vors_texture, ivec2(idxx + 10, idxy), vec4(0));//halen
			return;
		}

		//-----
		float gip = distance(vec2(xx, yy), vec2(vors.oldx, vors.oldy));
		float t = 1 / gip;
		if (t > 1) { t = 1; }

		dxf = (vors.oldx*(1 - t) + xx*t)-vors.oldx;
		dyf = (vors.oldy*(1 - t) + yy*t)-vors.oldy;
	
		imageStore(vors_texture, ivec2(idxx + 0, idxy), vec4(int(gip)));//finn
		imageStore(vors_texture, ivec2(idxx + 1, idxy), vec4(1));//curn
		imageStore(vors_texture, ivec2(idxx + 2, idxy), vec4(dxf));//dxf
		imageStore(vors_texture, ivec2(idxx + 3, idxy), vec4(dyf));//dyf

	}else{
		float finn=imageLoad(vors_texture, ivec2(idxx + 0, idxy)).r;//finn
		float curn=imageLoad(vors_texture, ivec2(idxx + 1, idxy)).r;//curn
		if (curn >= finn) { return; }
		curn += 1;
		imageStore(vors_texture, ivec2(idxx + 1, idxy), vec4(curn));//curn

		dxf=imageLoad(vors_texture, ivec2(idxx + 2, idxy)).r;//dxf
		dyf=imageLoad(vors_texture, ivec2(idxx + 3, idxy)).r;//dyf
		vors.oldx = imageLoad(vors_texture, ivec2(idxx + 8, idxy)).r;
		vors.oldy = imageLoad(vors_texture, ivec2(idxx + 9, idxy)).r;
	}
	
	
	//--------------------------------------------------------------

	

	float x = vors.oldx + dxf;
	float y = vors.oldy + dyf;
	imageStore(vors_texture, ivec2(idxx + 8, idxy), vec4(x));
	imageStore(vors_texture, ivec2(idxx + 9, idxy), vec4(y));

	if (!brokang) { return; }
	//////////--------------------grN-----------------------------------------
	vors.grN = int(imageLoad(vors_texture, ivec2(idxx + 4, idxy)).r);
	vors.alfa = imageLoad(vors_texture, ivec2(idxx + 5, idxy)).r;

	
	float dv = 1.5;
	float k = 0;


	k = 1536.0/pow(1.5, 14 - vors.grN);


	float raz = k - k / dv;
	
	k = (sz - k / dv) / raz;
	if (k > 1) { k = 1; }
	vors.alfa *= k;
	if (vors.alfa < 0) { return; }
	
	
	//----------------------------------------------------------------

	int px = int(x) - tex_offset_x;
	int py = int(y) - tex_offset_y;

	if (x < 0 || y < 0 || x >= docc_width - 1 || y >= docc_height - 1) { return; }
	//---------------------------------------------------------------
	vors.halen = bool(imageLoad(vors_texture, ivec2(idxx + 10, idxy)).r);
	vors.colrgba.r = imageLoad(vors_texture, ivec2(idxx + 11, idxy)).r;
	vors.colrgba.g = imageLoad(vors_texture, ivec2(idxx + 12, idxy)).r;
	vors.colrgba.b = imageLoad(vors_texture, ivec2(idxx + 13, idxy)).r;
	vors.colrgba.a = imageLoad(vors_texture, ivec2(idxx + 14, idxy)).r;
	if (brush_dirty || brush_grad_usesingle!=-1 || pickarea) {
		vors.colrigibiai.r = imageLoad(vors_texture, ivec2(idxx + 15, idxy)).r;
		vors.colrigibiai.g = imageLoad(vors_texture, ivec2(idxx + 16, idxy)).r;
		vors.colrigibiai.b = imageLoad(vors_texture, ivec2(idxx + 17, idxy)).r;
		vors.colrigibiai.a = imageLoad(vors_texture, ivec2(idxx + 18, idxy)).r;
	}
	

	//---------------------------------------------------------------
	//-----------------------------color-----------------------------
	//---------------------------------------------------------------

	

	color = getPixelFromLaytexture(px,py);
		ya = color.a;
	
	//*****************
	if (brush_pickunder) {

		vec4 undercolor = getPixelFromUndertexture(px, py);
		color = color*ya + undercolor*(1 - ya);
		
		vec4 ldataf= getPixelFromMtexture(px, py);
		ya = ldataf.a;

		color = color*(1 - ya) + ldataf*ya;
		color.a = 1;

	}
	else {
	//******************
		vec4 ldataf = getPixelFromMtexture(px, py);
		xa = ldataf.a;

		ta = xa + ya - xa*ya;

		if (ta != 0) {
			dec1 = ya*(1 - xa) / ta;
			dec2 = xa / ta;
			
			color = color*dec1 + ldataf*dec2;
		}

		color = color*ta + brush_curcolor*(1 - ta);
		color.a = ta;
	}
	//*********************dirty*************************
	if (brush_dirty) {
		if (!vors.halen) {

			float at = brush_dirtystrength_cur;
			color = vors.colrdgdbdad = vors.colrgba*(1 - at) + vors.colrigibiai*at;
			color.a = vors.colrdgdbdad.a = vors.colrgba.a*(1 - at) + brush_curcolor.a*at;
	
			
			if (!brush_preservedirty) {
				vors.colrigibiai = vors.colrdgdbdad;
				imageStore(vors_texture, ivec2(idxx + 15, idxy), vec4(vors.colrigibiai.r));
				imageStore(vors_texture, ivec2(idxx + 16, idxy), vec4(vors.colrigibiai.g));
				imageStore(vors_texture, ivec2(idxx + 17, idxy), vec4(vors.colrigibiai.b));
				imageStore(vors_texture, ivec2(idxx + 18, idxy), vec4(vors.colrigibiai.a));
			}
		}


		//a2 = brush->vors[i].ad;
	}
	else {
		if (brush_grad_usesingle == -1 && !pickarea) {
			vors.colrigibiai = brush_curcolor;
		}
		else {
			vors.colrigibiai = vors.colrigibiai*brush_grad_cur + brush_curcolor*(1 - brush_grad_cur);
		}
		
	}

	
	//***********************************
	if (vors.halen) {

		tmpf = brush_hale_cur;

		
		if (dcd_select) {
			tmpf += 1 - getPixelFromSelecttexture(px, py);;
			if (tmpf>1) { tmpf = 1; }
		}
		
		color = color*tmpf + vors.colrgba*(1 - tmpf);
		
	}
	
	//----------texture---------------------
	float ga = 1;
	if (brush_enabletexture) {

		float atm = 1;

		
	
		ga = texture(brtexTexture, vec2(x + brush_texoffsetx, y + brush_texoffsety) / brush_texscale_cur).r;
	
		if (!brush_texinvert) { ga = 1 - ga; }


		if (brush_texturemode == 3) {
			ga = 1 - ga*brush_texeffect_cur;
			ga = contrast(brush_texcontrast_cur, ga);//contrast
			atm *= ga;
		}//mult

		if (brush_texturemode == 50) {
			ga = contrast(brush_texcontrast_cur, ga);//contrast
			ga = 1-ga*(1 - vors.alfa*brush_texeffect_cur); 
			atm -= ga;
			if (atm<0) { atm = 0; }
		}//height

		if (brush_texturemode == 21) {
			ga = contrast(brush_texcontrast_cur, ga);//contrast
			ga *= brush_texeffect_cur;
			atm -= ga; if (atm<0) { atm = 0; }
		}//substract

	
	
		if (vors.halen) {
			vors.colrgba = vors.colrgba*(1 - atm) + color*(atm);
		}
		else {
			vors.colrgba = color;
		}
		
	
	}
	else {

		//**********************
		vors.colrgba = color;

		//**********************
	}

	

	tmpf = brush_coloramount_cur;

	//-----F-------------
	if (maincolor.r != -1) {
		if (brush_dirty && !brush_preservedirty) {
			vors.colrgba = maincolor;
			vors.colrigibiai = maincolor;
			color = maincolor;
			imageStore(vors_texture, ivec2(idxx + 15, idxy), vec4(vors.colrigibiai.r));
			imageStore(vors_texture, ivec2(idxx + 16, idxy), vec4(vors.colrigibiai.g));
			imageStore(vors_texture, ivec2(idxx + 17, idxy), vec4(vors.colrigibiai.b));
			imageStore(vors_texture, ivec2(idxx + 18, idxy), vec4(vors.colrigibiai.a));
		}
		else {
			tmpf = 1;
			vors.colrgba = vors.colrigibiai;
		}
	}
	//--------------

	vec4 color1 = vors.colrigibiai*tmpf + color*(1 - tmpf);

	//********************bristb*********
	if (brush_bristb_cur > 0.01) {
		float tmp = float(gl_GlobalInvocationID.x + vors_layer_offset) / brush_vorscount;
		float bb = brush_bristb_cur*tmp;
		if (float(gl_GlobalInvocationID.x) / 2.0 == gl_GlobalInvocationID.x / 2) { bb = -bb; }

		color1.r += bb*(color1.r);
		color1.g += bb*(color1.g);
		color1.b += bb*(color1.b);

		if (color1.r > 1) { color1.r = 1; }
		if (color1.g > 1) { color1.g = 1; }
		if (color1.b > 1) { color1.b = 1; }
		if (color1.r < 0) { color1.r = 0; }
		if (color1.g < 0) { color1.g = 0; }
		if (color1.b < 0) { color1.b = 0; }

	}
	//*************************************

	
	float a2 = brush_curcolor.a;//+++++++++++++++++++++++++++++++++++++++++

	float a1 = tmpf*a2 + color.a*(1 - tmpf);

	a1 *= brush_curcolor.a;
	
	a1 *= vors.alfa;

	if (brush_enabletexture) {
		if (brush_texturemode == 21) {
			a1 -= ga;
			if (a1<0) { a1 = 0; }
		}
		else {
			a1 *= ga;
		}
	}//texture
	//----------------------------------------------------------------
	float dx = (x - int(x));
	float dy = (y - int(y));

	float p00 = (1 - dx)*(1 - dy);
	float p01 = (1 - dx)*(dy);
	float p11 = (dx)*(dy);
	float p10 = (dx)*(1 - dy);
	
	float xa = 1;


	
		
	vec4 col = vec4(0, 0, 0, 0);

	
	//------------------for--------------------------
	

		for (int i = 0; i < 4; i++) {
			
			if (i == 0) { xa = p00; }
			if (i == 1) { xa = p01; py+=1;}
			if (i == 2) { xa = p11; px+=1;}
			if (i == 3) { xa = p10; py-=1;}

			xa *= a1;

			
			vec4 ldataf = getPixelFromMtexture(px, py);
		

			float ya = ldataf.a;

			//-----transparency-----------
			bool fltr = true;
			float tr = brush_transparency_cur;

			if (tr < 0.99) {
				fltr = true;

				if (ya > tr) { fltr = false; }

				if (fltr) {
					ya /= tr;
				}
			}
			//----------------------------

			float ta = xa + ya - ya*xa;

			if (ta != 0) {
				float dec1 = ya*(1 - xa) / ta;
				float dec2 = xa / ta;
				ldataf = ldataf*dec1 + color1*dec2;

			
				//-------transparency-----
				if (ta<ya) { ta = ya; }

				if (fltr || tr >= 0.99) {
					if (tr < 0.99) { ta *= tr; }
					ldataf.a = ta;
				}
				else {
					ldataf.a = ya;
				}
				//----------------
				
				storePixelToMtexture(px, py, ldataf);
			}
			
		
		}
		
		//-----------------------

		if (tmpf != -1) { vors.halen = true; }

		imageStore(vors_texture, ivec2(idxx + 10, idxy), vec4(vors.halen));

		imageStore(vors_texture, ivec2(idxx + 11, idxy), vec4(vors.colrgba.r));
		imageStore(vors_texture, ivec2(idxx + 12, idxy), vec4(vors.colrgba.g));
		imageStore(vors_texture, ivec2(idxx + 13, idxy), vec4(vors.colrgba.b));
		imageStore(vors_texture, ivec2(idxx + 14, idxy), vec4(vors.colrgba.a));
		

	
}