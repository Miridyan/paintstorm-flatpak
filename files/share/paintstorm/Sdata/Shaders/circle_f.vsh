//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;


uniform sampler2D formTexture;
uniform sampler2D mTexture;
uniform sampler2D blendTexture;
uniform sampler2D blurTexture;
uniform sampler2D brtexTexture;

uniform sampler1D brgradTexture;


uniform float XX;
uniform float YY;

uniform float sz;
uniform float texsize;
uniform int border;

uniform bool blur_enable;


//--------brush----
uniform float brush_opacity_cur;
uniform float brush_transparency_cur;
uniform int brush_brmode;
uniform bool brush_enabletexture;
uniform int brush_texture_size;
uniform float brush_texscale_cur;
uniform float brush_texoffsetx;
uniform float brush_texoffsety;
uniform bool brush_texinvert;
uniform int brush_texturemode;
uniform float brush_texeffect_cur;
uniform float brush_texcontrast_cur;
uniform float brush_coloramount_cur;
uniform vec4 brush_curcolor;
uniform int brush_grad_usesingle;
uniform float brush_grad_cur;
uniform bool brush_formsdvig_affectgrads;
uniform float brush_formsdvig_rad;
uniform float brush_formsdvig_xoffset;
uniform float brush_formsdvig_yoffset;
uniform float brush_blur_cur;
uniform float brush_hale_cur;
//------------
uniform float cosm;
uniform float sinm;
uniform float cosm2;
uniform float sinm2;
uniform float brush_squeezex;
uniform float brush_squeezey;


uniform float X1;
uniform float Y1;
uniform float dx;
uniform float dy;
uniform float size;
////////////////////////////////////
vec4 bilinear(sampler2D tex, vec2 cor) {

	
	float dx = cor.x - int(cor.x);
	float dy = cor.y - int(cor.y);

	cor.x = int(cor.x) + 0.5;
	cor.y = int(cor.y) + 0.5;

	vec4 col0, col1, col2, col3;
	col0 = col1 = col2 = col3 = vec4(0);

	if (dx <= 0.5 && dy <= 0.5) {
		col0 = texture2D(tex, vec2(cor.x - 1, cor.y - 1) / texsize);
		col1 = texture2D(tex, vec2(cor.x, cor.y - 1) / texsize);
		col2 = texture2D(tex, vec2(cor.x, cor.y) / texsize);
		col3 = texture2D(tex, vec2(cor.x - 1, cor.y) / texsize);
		dx += 0.5;
		dy += 0.5;
	}
	else
		if (dx >= 0.5 && dy <= 0.5) {
			col0 = texture2D(tex, vec2(cor.x, cor.y - 1) / texsize);
			col1 = texture2D(tex, vec2(cor.x + 1, cor.y - 1) / texsize);
			col2 = texture2D(tex, vec2(cor.x + 1, cor.y) / texsize);
			col3 = texture2D(tex, vec2(cor.x, cor.y) / texsize);
			dx -= 0.5;
			dy += 0.5;
		}
		else
			if (dx <= 0.5 && dy >= 0.5) {
				col0 = texture2D(tex, vec2(cor.x - 1, cor.y) / texsize);
				col1 = texture2D(tex, vec2(cor.x, cor.y) / texsize);
				col2 = texture2D(tex, vec2(cor.x, cor.y + 1) / texsize);
				col3 = texture2D(tex, vec2(cor.x - 1, cor.y + 1) / texsize);
				dx += 0.5;
				dy -= 0.5;
			}
			else
				if (dx >= 0.5 && dy >= 0.5) {
					col0 = texture2D(tex, vec2(cor.x, cor.y) / texsize);
					col1 = texture2D(tex, vec2(cor.x + 1, cor.y) / texsize);
					col2 = texture2D(tex, vec2(cor.x + 1, cor.y + 1) / texsize);
					col3 = texture2D(tex, vec2(cor.x, cor.y + 1) / texsize);
					dx -= 0.5;
					dy -= 0.5;
				}




	if (col0.a == 0 || col1.a == 0 || col2.a == 0 || col3.a == 0) {

		float sum = col0.a + col1.a + col2.a + col3.a;
		if (sum != 0) {

			vec4 midcol = (col0*col0.a + col1*col1.a + col2*col2.a + col3*col3.a) / sum;

			col0.r = col0.r*col0.a + midcol.r*(1 - col0.a);
			col0.g = col0.g*col0.a + midcol.g*(1 - col0.a);
			col0.b = col0.b*col0.a + midcol.b*(1 - col0.a);

			col1.r = col1.r*col1.a + midcol.r*(1 - col1.a);
			col1.g = col1.g*col1.a + midcol.g*(1 - col1.a);
			col1.b = col1.b*col1.a + midcol.b*(1 - col1.a);

			col2.r = col2.r*col2.a + midcol.r*(1 - col2.a);
			col2.g = col2.g*col2.a + midcol.g*(1 - col2.a);
			col2.b = col2.b*col2.a + midcol.b*(1 - col2.a);

			col3.r = col3.r*col3.a + midcol.r*(1 - col3.a);
			col3.g = col3.g*col3.a + midcol.g*(1 - col3.a);
			col3.b = col3.b*col3.a + midcol.b*(1 - col3.a);
		}
		else {
			return vec4(0, 0, 0, 0);
		}

	}

	float p0 = (1 - dx)*(1 - dy);
	float p1 = (dx)*(1 - dy);
	float p2 = (dx)*(dy);
	float p3 = (1 - dx)*(dy);

	return   (col0*p0 + col1*p1 + col2*p2 + col3*p3);

}
////////////////////////////////////
///////////////////////////////////////
vec4 getGradColor(float gip) {

	
	if (gip > 1) { gip = 1; }

	float num = 0;
	 num = texture(brgradTexture, 0).r;
	
		
	float li = 0;
	
	for (int i = 0; i < num; i++) {
		if (texture(brgradTexture, float(i + 1) / 256).g > gip) {
			break;
		}
		li = i;
	}

	float posl = texture(brgradTexture, (li + 1) / 256).g;
	float posr = texture(brgradTexture, (li + 2) / 256).g;
	
	vec4 coll = texture(brgradTexture, (li + 1 + num) / 256);
	vec4 colr = texture(brgradTexture, (li + 2 + num) / 256);

	float dec = gip - posl;
	
	if (dec > 0) {
		dec /= posr - posl;
	}
	else {
		dec = 0;
	}

	if (dec < 0) { dec = 0; }
	if (dec > 1) { dec = 1; }
	
	vec4 res = coll*(1 - dec) + colr*(dec);

	
	
	return res;
}
////////////////////////////////////////

float contrast(float contrast, float ga) {
	float cc = contrast*0.5;
	float cc1 = 1 - cc;
	
	if (ga<cc) {
		ga -= (cc - ga);
		if (ga<0) { ga = 0; }
		return ga;
	}
	if (ga>cc1) {
		ga += (ga - cc1);
		if (ga>1) { ga = 1; }
	}
	return ga;
}

/////////////////////////////////////////////////////////
void main() {
	
	
	vec2 rot = texCoords;

	float alpha;

	 rot = (texCoords.xy*size + vec2(dx, dy) - vec2(border, border)) / sz;
	if (brush_brmode == 2) {
		float ka = 0.5 - rot.x;
		float kb = 0.5 - rot.y;

		rot.x = ka*cosm - kb*sinm;
		rot.y = ka*sinm + kb*cosm;

		rot.x *= brush_squeezex;
		rot.y *= brush_squeezey;

		rot.x += 0.5;
		rot.y += 0.5;

		
	}

	alpha = texture2D(formTexture, rot).r;

	
	
			
	vec4 col = texture2D(mTexture, texCoords*size/texsize + vec2(int(XX), int(YY))/texsize);

	if (alpha == 0) {
		FragColor = col;
		return;
	}

	vec4 cvet;


	if (brush_coloramount_cur < 1) {
		//---------blur-----------------------
		if (blur_enable) {

			vec2 bcor;
			if (brush_hale_cur > 0.5) {
				bcor = vec2(round(dx), round(dy)) - vec2(1);
			}
			else {
				bcor = vec2(dx, dy) - vec2(1);
			}
			
			cvet = bilinear(blurTexture, texCoords*size+bcor);
			cvet = cvet*(1 - brush_coloramount_cur) + brush_curcolor*brush_coloramount_cur;
		
		}
		else {
			//--------------normal color----------------
				cvet = texture2D(blendTexture, vec2(2, 1023) / 1024);
		}

	}
	else {
		cvet = brush_curcolor;
	}
	//-------------------------------------


	float xa = alpha*brush_opacity_cur*cvet.a;

	//----------texture---------------------
	if (brush_enabletexture) {

		float sc =  brush_texture_size / brush_texscale_cur;
		float ga=texture2D(brtexTexture, (texCoords*size + vec2(int(X1)+dx, int(Y1)+dy) + vec2(brush_texoffsetx, brush_texoffsety))*sc / brush_texture_size).r;
		if (!brush_texinvert) { ga = 1 - ga; }

		
		if (brush_texturemode == 3) {
			ga = 1 - ga*brush_texeffect_cur;
			ga=contrast(brush_texcontrast_cur,ga);//contrast
			xa *= ga;
		}//mult

		if (brush_texturemode == 50) {
			ga=contrast(brush_texcontrast_cur,ga);//contrast
			xa -= ga*(1 - xa*brush_texeffect_cur); if (xa<0) { xa = 0; }
		}//height

		if (brush_texturemode == 21) {
			ga=contrast(brush_texcontrast_cur, ga);//contrast
			ga *= brush_texeffect_cur;
			xa -= ga; if (xa<0) { xa = 0; }
		}//substract
		
		
	}
	//--------------------------
	//---------grad-------------
	if (brush_grad_usesingle != -1) {//grad

		vec4 rgbaf=vec4(0,0,0,0);

		if (brush_grad_usesingle == 0) {

			float tx = 0.5;
			float ty = 0.5;
			float m = 2;

			if (brush_formsdvig_affectgrads) {
				m /= ((brush_formsdvig_rad + 40) / 40);
				tx -= brush_formsdvig_xoffset*40/sz;
				ty -= brush_formsdvig_yoffset*40/sz;
			}

			float gip = distance(texCoords.xy, vec2(tx - dx / size, ty - dy / size))*m;
			if (brush_brmode == 2) { gip *= 1.4; }
			rgbaf = getGradColor(gip);
		}
		else {
			
			float ka = (0.5 - dx / sz)*sz / size - texCoords.x;
			float kb = (0.5 - dy / sz)*sz / size - texCoords.y;
			float xx = ka*cosm2 - kb*sinm2;

			if (brush_brmode == 2) { xx *= 1.4; }

			rgbaf = getGradColor(1-abs(0.5 - xx));	
		}

		cvet = rgbaf *brush_grad_cur + cvet*(1 - brush_grad_cur);
		
		xa *= rgbaf.a;
	
	}
	//---------------------------
	
	float ya = col.a;

	//-----transparency-----------
	bool fltr = true;
	float tr = brush_transparency_cur;

	if (tr < 0.99) {
		fltr = true;

		if (ya > tr) { fltr = false; }

		if (fltr) {
			ya /= tr;
		}
	}
	//----------------------------

	float ta = xa + ya - ya*xa;
	if (ta != 0) {
		float dec1 = ya*(1 - xa) / ta;
		float dec2 = xa / ta;
		cvet = col*dec1 + cvet*dec2;
		cvet.a = ta;
	}
	else {
		cvet.a = 0;
	}

	//-------transparency-----

	if (ta<ya) { ta = ya; }


	if (fltr || tr >= 0.99) {
		if (tr < 0.99) { ta *= tr; }
		cvet.a = ta;
	}
	else {
		cvet.a = ya;
	}


	//----------------
	
	
	
	FragColor = cvet;
		
	
	
}
