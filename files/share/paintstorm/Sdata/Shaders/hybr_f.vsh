//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;


uniform sampler2D undertex;
uniform sampler2D laytex;
uniform sampler2D mtex;
uniform sampler2D selecttex;

uniform int layer_blendmode;
uniform int brush_blendmode;
uniform float layer_opacity;
uniform bool dcd_select;
uniform bool brush_eraser;
uniform bool layer_preserveopacity;


#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) < (b) ? (b) : (a))
/////////////////////////////////////////////
///////////////////////////////
float Max(vec4 C) {
	if (C.r>C.g && C.r>C.b) { return C.r; }
	else {
		if (C.g>C.b) { return C.g; }
		else { return C.b; }
	}
}
///////////////////////////////
float Min(vec4 C) {
	if (C.r<C.g && C.r<C.b) { return C.r; }
	else {
		if (C.g<C.b) { return C.g; }
		else { return C.b; }
	}
}
/////////////////////////////////
float Lum(vec4 C) {
	return (C.r * 0.3f + C.g * 0.59f + C.b * 0.11f);
}
///////////////////////////////////
vec4 ClipColor(vec4 C) {
	float l = Lum(C);
	float n = Min(C);
	float x = Max(C);
	if (n<0) {
		C.r = l + (((C.r - l)*l) / (l - n));
		C.g = l + (((C.g - l)*l) / (l - n));
		C.b = l + (((C.b - l)*l) / (l - n));
	}
	if (x>1) {
		C.r = l + (((C.r - l)*(1 - l)) / (x - l));
		C.g = l + (((C.g - l)*(1 - l)) / (x - l));
		C.b = l + (((C.b - l)*(1 - l)) / (x - l));
	}
	return C;
}
///////////////////////////////////////
vec4 SetLum(vec4 C, float l) {
	float d = l - Lum(C);
	C.r += d;
	C.g += d;
	C.b += d;
	C=ClipColor(C);
	return C;
}
///////////////////////////////////////
float Sat(vec4 C) {
	return (Max(C) - Min(C));
}
///////////////////////////////////////
vec4 SetSat(vec4 CC, float s) {

	float C[3];
	C[0] = CC.r;
	C[1] = CC.g;
	C[2] = CC.b;

	int max = 0;
	int mid = 0;
	int min = 0;
	float maxc = Max(CC);
	float minc = Min(CC);

	for (int i = 0; i<3; i++) {
		if (C[i] == maxc) { max = i; break; }
	}
	for (int i = 0; i<3; i++) {
		if (C[i] == minc && i != max) { min = i; break; }
	}
	for (int i = 0; i<3; i++) {
		if (i != max && i != min) { mid = i; break; }
	}

	if (C[max]>C[min]) {
		C[mid] = ((C[mid] - C[min])*s) / (C[max] - C[min]);
		C[max] = s;
	}
	else {
		C[mid] = 0;
		C[max] = 0;
	}
	C[min] = 0;

	CC.r = C[0];
	CC.g = C[1];
	CC.b = C[2];

	return CC;
	
}
//////////////////////////////////////////////////
float rand(void){
	return fract(sin(dot(texCoords, vec2(12.9898, 78.233)))*43758.5453);
}
///////////////////////////////////////////////////
vec4 blend(vec4 colxa, vec4 colya, float a, float b, int blendmode) {

	if (blendmode == 0) {//BLEND_NORMAL
		return colxa*a+colya*b;
	}

	


	switch (blendmode) {



	case 22://BLEND_DIVIDE
	{
		vec4 tmp = colya;
		if (colxa.r != 0) { tmp.r = colya.r / colxa.r; }
		if (colxa.g != 0) { tmp.g = colya.g / colxa.g; }
		if (colxa.b != 0) { tmp.b = colya.b / colxa.b; }
		if (tmp.r < 0) { tmp.r = 0; }
		if (tmp.g < 0) { tmp.g = 0; }
		if (tmp.b < 0) { tmp.b = 0; }
		if (tmp.r > 1) { tmp.r = 1; }
		if (tmp.g > 1) { tmp.g = 1; }
		if (tmp.b > 1) { tmp.b = 1; }

		colya = tmp*a + colya * b;
	}
		break;



	case 21://BLEND_SUBSTRACT:
	{
		colya = colya - colxa*a;
		if (colya.r < 0) { colya.r = 0; }
		if (colya.g < 0) { colya.g = 0; }
		if (colya.b < 0) { colya.b = 0; }
	
	}
		break;

	case 20://BLEND_EXCLUSION:
		colya = colxa*a + colya - 2 * colxa*a*colya;
		break;

	case 18://BLEND_HARDMIX:
		//++++++++++++++++++++++++++++++
		if (colya.r > 1 - colxa.r) {
			colya.r = a + colya.r*b;
		}
		else { colya.r = colya.r*b; }

	if (colya.g > 1 - colxa.g) {
		colya.g = a + colya.g*b;
	}
	else { colya.g = colya.g*b; }

	if (colya.b > 1 - colxa.b) {
		colya.b = a + colya.b*b;
	}
	else { colya.b = colya.b*b; }

		break;


		
	case 17://BLEND_PINLIGHT:

		if (colxa.r < 0.5) {
			if (2 * colxa.r<colya.r) { colya.r = 2 * colxa.r * a + colya.r * b; }
		}
		else {
			if (2 * (colxa.r - 0.5)>colya.r) { colya.r = 2 * (colxa.r - 0.5)*a + colya.r * b; }
		}

		if (colxa.g < 0.5) {
			if (2 * colxa.g<colya.g) { colya.g = 2 * colxa.g * a + colya.g * b; }
		}
		else {
			if (2 * (colxa.g - 0.5)>colya.g) { colya.g = 2 * (colxa.g - 0.5)*a + colya.g * b; }
		}

		if (colxa.b < 0.5) {
			if (2 * colxa.b<colya.b) { colya.b = 2 * colxa.b * a + colya.b * b; }
		}
		else {
			if (2 * (colxa.b - 0.5)>colya.b) { colya.b = 2 * (colxa.b - 0.5)*a + colya.b * b; }
		}

	

		break;
		
	case 16://BLEND_LINEARLIGHT:

		if (colxa.r<0.5) {
			float cc = 2 * colxa.r * a + b;
			if (cc + colya.r<1) {
				colya.r = 0;
			}
			else {
				colya.r = (cc + colya.r - 1);
			}
		}
		else {
			colya.r = MIN(1, 2 * (colxa.r - 0.5)*a + colya.r);
		}

		if (colxa.g<0.5) {
			float cc = 2 * colxa.g * a + b;
			if (cc + colya.g<1) {
				colya.g = 0;
			}
			else {
				colya.g = (cc + colya.g - 1);
			}
		}
		else {
			colya.g = MIN(1, 2 * (colxa.g - 0.5)*a + colya.g);
		}

		if (colxa.b<0.5) {
			float cc = 2 * colxa.b * a + b;
			if (cc + colya.b<1) {
				colya.b = 0;
			}
			else {
				colya.b = (cc + colya.b - 1);
			}
		}
		else {
			colya.b = MIN(1, 2 * (colxa.b - 0.5)*a + colya.b);
		}

	

		break;
		
	case 15://BLEND_VIVIDLIGHT:
		if (colxa.r<0.5) {
			float cc = 2 * colxa.r * a + b;
			if (cc != 0) {
				float tmp = 1 - (1 - colya.r) / cc;
				if (tmp<0) { tmp = 0; }if (tmp>1) { tmp = 1; }
				colya.r = tmp;
			}
			else {
				if (colya.r != 1) { colya.r = 0; }
			}
		}
		else {
			if (2 * (colxa.r - 0.5)*a != 1) {
				float tmp = (colya.r) / (1 - 2 * (colxa.r - 0.5)*a);
				if (tmp>1) { tmp = 1; }
				colya.r = tmp;
			}
			else { colya.r = 1; }
		}

		if (colxa.g<0.5) {
			float cc = 2 * colxa.g * a + b;
			if (cc != 0) {
				float tmp = 1 - (1 - colya.g) / cc;
				if (tmp<0) { tmp = 0; }if (tmp>1) { tmp = 1; }
				colya.g = tmp;
			}
			else {
				if (colya.g != 1) { colya.g = 0; }
			}
		}
		else {
			if (2 * (colxa.g - 0.5)*a != 1) {
				float tmp = (colya.g) / (1 - 2 * (colxa.g - 0.5)*a);
				if (tmp>1) { tmp = 1; }
				colya.g = tmp;
			}
			else { colya.g = 1; }
		}

		if (colxa.b<0.5) {
			float cc = 2 * colxa.b * a + b;
			if (cc != 0) {
				float tmp = 1 - (1 - colya.b) / cc;
				if (tmp<0) { tmp = 0; }if (tmp>1) { tmp = 1; }
				colya.b = tmp;
			}
			else {
				if (colya.b != 1) { colya.b = 0; }
			}
		}
		else {
			if (2 * (colxa.b - 0.5)*a != 1) {
				float tmp = (colya.b) / (1 - 2 * (colxa.b - 0.5)*a);
				if (tmp>1) { tmp = 1; }
				colya.b = tmp;
			}
			else { colya.b = 1; }
		}

		
	
		break;
		
	case 13://BLEND_SOFTLIGHT:
	
		if (colxa.r<0.5) {
			colya.r =  (colya.r - (1 - 2 * colxa.r)*colya.r*(1 - colya.r))*a + b*colya.r;
		}
		else {
			float D;
			if (colya.r <= 0.25) {
				D = ((16 * colya.r - 12)*colya.r + 4)*colya.r;
			}
			else {
				D = sqrt(colya.r);
			}
			colya.r =  (colya.r + (2 * colxa.r - 1)*(D - colya.r))*a + b*colya.r;
		}

		if (colxa.g<0.5) {
			colya.g = (colya.g - (1 - 2 * colxa.g)*colya.g*(1 - colya.g))*a + b*colya.g;
		}
		else {
			float D;
			if (colya.g <= 0.25) {
				D = ((16 * colya.g - 12)*colya.g + 4)*colya.g;
			}
			else {
				D = sqrt(colya.g);
			}
			colya.g = (colya.g + (2 * colxa.g - 1)*(D - colya.g))*a + b*colya.g;
		}

		if (colxa.b<0.5) {
			colya.b = (colya.b - (1 - 2 * colxa.b)*colya.b*(1 - colya.b))*a + b*colya.b;
		}
		else {
			float D;
			if (colya.b <= 0.25) {
				D = ((16 * colya.b - 12)*colya.b + 4)*colya.b;
			}
			else {
				D = sqrt(colya.b);
			}
			colya.b = (colya.b + (2 * colxa.b - 1)*(D - colya.b))*a + b*colya.b;
		}
	
		
		break;
		
	case 14://BLEND_HARDLIGHT:
		if (colxa.r<0.5) {
			colya.r = 2 * colxa.r * colya.r*a + b*colya.r;
		}
		else {
			colya.r = (1 - (1 - (2 * colxa.r - 1))*(1 - colya.r))*a + colya.r * b;
		}

		if (colxa.g<0.5) {
			colya.g = 2 * colxa.g * colya.g*a + b*colya.g;
		}
		else {
			colya.g = (1 - (1 - (2 * colxa.g - 1))*(1 - colya.g))*a + colya.g * b;
		}

		if (colxa.b<0.5) {
			colya.b = 2 * colxa.b * colya.b*a + b*colya.b;
		}
		else {
			colya.b = (1 - (1 - (2 * colxa.b - 1))*(1 - colya.b))*a + colya.b * b;
		}

		
	
		break;

		

	case 12://BLEND_OVERLAY:

		if (colya.r<0.5) {
			colya.r = 2 * colxa.r * colya.r*a + b*colya.r;
		}
		else {
			colya.r = (1 - (1 - (2 * colya.r - 1))*(1 - colxa.r))*a + colya.r * b;
		}

		if (colya.g<0.5) {
			colya.g = 2 * colxa.g * colya.g*a + b*colya.g;
		}
		else {
			colya.g = (1 - (1 - (2 * colya.g - 1))*(1 - colxa.g))*a + colya.g * b;
		}

		if (colya.b<0.5) {
			colya.b = 2 * colxa.b * colya.b*a + b*colya.b;
		}
		else {
			colya.b = (1 - (1 - (2 * colya.b - 1))*(1 - colxa.b))*a + colya.b * b;
		}
		

		break;

	case 11://BLEND_LIGHTERCOLOR:
		if (colxa.r * 0.3 + colxa.g * 0.59 + colxa.b * 0.11>colya.r * 0.3 + colya.g * 0.59 + colya.b * 0.11) {
			colya = colya*b + colxa*a;
		}
		
		break;

		
	case 1://BLEND_DISSOLVE:
	{
		float rnd = rand();
		if (rnd<a) {
			colya = colxa;
		}
		
	}
	break;
	
	case 2://BLEND_DARKEN:

		if (colxa.r<colya.r) {
			colya.r = colxa.r * a + colya.r * b;
		}
		if (colxa.g<colya.g) {
			colya.g = colxa.g * a + colya.g * b;
		}
		if (colxa.b<colya.b) {
			colya.b = colxa.b * a + colya.b * b;
		}
		
		break;

	case 3://BLEND_MULTIPLY:
		colya = colxa*colya*a + b*colya;
	
		break;

	case 4://BLEND_COLORBURN:
	{
		float cc, tmp;

		cc = colxa.r * a + b;
		if (cc != 0) {
			tmp = 1 - (1 - colya.r) / cc;
			if (tmp<0) { tmp = 0; }if (tmp>1) { tmp = 1; }
			colya.r = tmp;
		}
		else {
			if (colya.r != 1) { colya.r = 0; }
		}

		cc = colxa.g * a + b;
		if (cc != 0) {
			tmp = 1 - (1 - colya.g) / cc;
			if (tmp<0) { tmp = 0; }if (tmp>1) { tmp = 1; }
			colya.g = tmp;
		}
		else {
			if (colya.g != 1) { colya.g = 0; }
		}

		cc = colxa.b * a + b;
		if (cc != 0) {
			tmp = 1 - (1 - colya.b) / cc;
			if (tmp<0) { tmp = 0; }if (tmp>1) { tmp = 1; }
			colya.b = tmp;
		}
		else {
			if (colya.b != 1) { colya.b = 0; }
		}
		
	}
	break;

	
	case 5://BLEND_LINEARBURN:
	{
		float cc;

		cc = colxa.r * a + b;
		if (cc + colya.r < 1) {
			colya.r = 0;
		}
		else {
			colya.r = cc + colya.r - 1;
		}

		cc = colxa.g * a + b;
		if (cc + colya.g < 1) {
			colya.g = 0;
		}
		else {
			colya.g = cc + colya.g - 1;
		}

		cc = colxa.b * a + b;
		if (cc + colya.b < 1) {
			colya.b = 0;
		}
		else {
			colya.b = cc + colya.b - 1;
		}
		
	}
		break;

		
	case 19://BLEND_DIFFERENCE:
		colya.r = abs(colxa.r*a - colya.r);
		colya.g = abs(colxa.g*a - colya.g);
		colya.b = abs(colxa.b*a - colya.b);
		
		break;

	case 6://BLEND_DARKERCOLOR:

		if (colxa.r * 0.3 + colxa.g * 0.59 + colxa.b * 0.11<colya.r * 0.3 + colya.g * 0.59 + colya.b * 0.11) {
			colya = colya*b + colxa* a;
		}
		break;

	case 7://BLEND_LIGHTEN:

		if (colxa.r>colya.r) {
			colya.r = colxa.r * a + colya.r * b;
		}

		if (colxa.b>colya.b) {
			colya.b = colxa.b * a + colya.b * b;
		}

		if (colxa.g>colya.g) {
			colya.g = colxa.g * a + colya.g * b;
		}

		break;

	case 8://BLEND_SCREEN:
	
			colya.r = (1 - (1 - colxa.r)*(1 - colya.r))*a + colya.r * b;
			colya.g = (1 - (1 - colxa.g)*(1 - colya.g))*a + colya.g * b;
			colya.b = (1 - (1 - colxa.b)*(1 - colya.b))*a + colya.b * b;
	
		break;

	case 9://BLEND_COLORDODGE:
	{
		float tmp;

			if (colxa.r * a != 1) {
				tmp = colya.r / (1 - colxa.r * a);
				if (tmp > 1) { tmp = 1; }
				colya.r = tmp;
			}
			else { colya.r = 1; }

			if (colxa.g * a != 1) {
				tmp = colya.g / (1 - colxa.g * a);
				if (tmp > 1) { tmp = 1; }
				colya.g = tmp;
			}
			else { colya.g = 1; }


			if (colxa.b * a != 1) {
				tmp = colya.b / (1 - colxa.b * a);
				if (tmp > 1) { tmp = 1; }
				colya.b = tmp;
			}
			else { colya.b = 1; }

	
	}
		break;

	case 10://BLEND_ADD:

		colya.r = MIN(1, colxa.r * a + colya.r);
		colya.g = MIN(1, colxa.g * a + colya.g);
		colya.b = MIN(1, colxa.b * a + colya.b);

		break;


	case 23://BLEND_HUE:
		colxa=SetSat(colxa, Sat(colya));
		colxa=SetLum(colxa, Lum(colya));
		colya = colxa*a + colya*b;
	break;

	
	case 25:// BLEND_COLOR:

		colxa=SetLum(colxa, colya.r * 0.3f + colya.g * 0.59f + colya.b * 0.11f);
		colya = colxa*a + colya*b;

	break;
	
	case 24://BLEND_SATURATION:
	{
		
		float lcb = Lum(colya);
		colxa=SetSat(colya, Sat(colxa));
		colxa=SetLum(colxa, lcb);

		colya = colxa*a + colya*b;
		
	}
	break;
	
	case 26://BLEND_LUMINOSITY:

		colxa=SetLum(colya, Lum(colxa));

		colya = colxa*a + colya*b;
		
	break;

	
	}

	return colya;

}
///////////////////////////////////////////////////
void main() {

	vec4 colxa = texture2D(mtex, texCoords);
	vec4 colya = texture2D(laytex, texCoords);



	if (dcd_select) {
		colxa.a *= texture2D(selecttex, texCoords).r;
	}
	

	float xa = colxa.a;
	float ya = colya.a;



	if (brush_eraser) {

		
		ya *= 1 - xa;
		if (ya<0.0001) { ya = 0; }

		ya *= layer_opacity;

		
		if (layer_blendmode == 0) {
			FragColor = colya*ya + texture2D(undertex, texCoords)*(1 - ya);
		}
		else {
			FragColor = blend(colya, texture2D(undertex, texCoords), ya, 1 - ya, layer_blendmode);
		}

	}
	else {
		float ta = xa + ya - xa*ya;
		float dec1 = 0;
		float dec2 = 0;

		if (ta != 0) {
			dec1 = ya*(1 - xa) / ta;
			dec2 = xa / ta;
		}
		//------------
		if (brush_blendmode == 0) {
			colxa = colya*dec1 + colxa*dec2;
		}
		else {
			vec4 col = colya*dec1 + colxa*dec2;
			colxa=blend(colxa, colya, dec2, dec1, brush_blendmode);
			colxa = colxa*ya + col*(1 - ya);
		}
		//------------------

		if (layer_preserveopacity) {
			if (ta > ya) { ta = ya;}
		}

		ta *= layer_opacity;

		
		if (layer_blendmode == 0) {
			FragColor = texture2D(undertex, texCoords)*(1 - ta) + colxa*ta;
		}
		else {
			FragColor = blend(colxa, texture2D(undertex, texCoords), ta, 1 - ta, layer_blendmode);
		}
		FragColor.a = 1;
	}
}
