//Copyright (C) 2014-2017 Paintstorm Studio
#version 150


in vec2 texCoords;
out vec4 FragColor;


uniform sampler2D tex;

uniform float texsize;
uniform int num;

void main() {

	if (num == 0) {
		FragColor = texture2D(tex, vec2(0.5,0.5)/texsize);
	}

	if (num == 1) {
		FragColor = texture2D(tex, vec2(texsize-0.5, 0.5) / texsize);
	}

	if (num == 2) {
		FragColor = texture2D(tex, vec2(texsize-0.5, texsize-0.5) / texsize);
	}

	if (num == 3) {
		FragColor = texture2D(tex, vec2(0.5, texsize-0.5) / texsize);
	}
	
	
}