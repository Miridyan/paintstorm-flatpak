//Copyright (C) 2014-2017 Paintstorm Studio
#version 150

in vec3 coord;
out vec2 texCoords; 


uniform mat4 VP; 


void main() {
	
	vec2 cor = coord.xy / 2 + vec2(0.5, 0.5);
	
	gl_Position = VP*vec4(cor.xy, 0.0, 1.0);

	texCoords = cor.xy;

}